# CHIMES Driver

Welcome to the CHIMES Driver repository. This python script can be used to run CHIMES as a stand-alone module.

For more information on CHIMES, please see the [main CHIMES home page](https://richings.bitbucket.io/chimes/home.html).

For more information on how to use CHIMES Driver, please see the [CHIMES User Guide](https://richings.bitbucket.io/chimes/user_guide/index.html).
